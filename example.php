
<?php 
#start year
$timest = strtotime("1980");
$year = date("Y", $timest );

#stop year
$time = strtotime("2018");
$stopyear = date("Y", $time);

#leap Year
$count = 0;

#to print year
while($year <= $stopyear){
    if(leapYear($year)){
        $count = $count + 1;
        echo $year . " Leap Year" . "<br>";
    }
    else{
        echo $year . "<br>";
    }

    #increment
    $year = $year + 1;
}

echo "<br>". "Total number of leap year ". $count;

#function of leap year
function leapYear($year){
    $leapYear=false;
    if ((($year % 4) == 0) && ((($year % 100) != 0) || (($year % 400) == 0))){
       $leapYear = true;
       return $leapYear;
    }

}
?>